import { AppContainer } from 'react-hot-loader';
import * as React from "react"
import { ReactGhLikeDiff } from "react-gh-like-diff";
import "react-gh-like-diff/lib/diff2html.min.css";
import DiffGetter from './DiffGetter';
const fs = require('fs');

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.diffString = fs.readFileSync("src/test.txt").toString();
        this.state={
            value: 1
        };
    }

    updateValue(e){
        console.log(this.state.value);
        this.setState({
            value: this.state.value+1
        });
    }

    render() {
        return (
            <AppContainer>
                <div>
                    <button onClick={(e)=>{this.updateValue(e)}}>Click</button>
                    <DiffGetter value={this.state.value}/>
                    <ReactGhLikeDiff
                        options={{
                            originalFileName: "a",
                            updatedFileName: "b",
                            outputFormat: "side-by-side",
                            showFiles: true
                        }}
                        diffString={this.diffString}
                    />
                </div></AppContainer>
        );
    }
}
