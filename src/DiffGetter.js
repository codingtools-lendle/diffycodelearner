import * as React from "react";
const os=require('os');
const fs=require('fs-extra');
const path=require('path');

export default class DiffGetter extends React.Component{
    constructor(){
        super();
        this.state={
            repo:"https://gitlab.com/codingtools-lendle/diffycodelearner.git",
            tagA:"0917795825585a26bcb6df283b67b234c6a3dd16",
            tagB:"19962aad64b8fd7dd00debba838e87e42a07dc7d"
        };
    }

    compareGit(e){
        console.log(os.tmpdir);
        let projectName=this.state.repo;
        projectName=projectName.substring(projectName.lastIndexOf("/")+1, projectName.lastIndexOf("."));
        let diffyTmp=path.join(os.tmpdir(), ".diffy");
        let projectPath=path.join(diffyTmp, projectName);
        if(!fs.existsSync(diffyTmp)){
            fs.mkdirSync(diffyTmp, {
                recursive: true
            });
        }
        if(!fs.existsSync(projectPath)){
            let simpleGit = require('simple-git')(diffyTmp);
            simpleGit.clone(this.state.repo, projectPath).then(function(){
                console.log("ok!");
                console.log(projectPath);
                fs.removeSync(projectPath);
            });
        }
    }

    render(){
        return (
            <div>
                <span>{this.props.value}</span>
                Repo: <input type="text" value={this.state.repo} onChange={(e)=>{this.setState({repo: e.target.value})}}/>
                A: <input type="text" value={this.state.targetA} onChange={(e)=>{this.setState({targetA: e.target.value})}}/>
                B: <input type="text" value={this.state.targetB} onChange={(e)=>{this.setState({targetB: e.target.value})}}/>
                <button onClick={(e)=>{this.compareGit(e)}}>Compare</button>
            </div>
        );
    }
}
